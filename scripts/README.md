# Liquid Staking test scripts

## Usage

You'll need a Node.JS v18+ environment installed on your machine that provides for *node*, *npm*, *yarn*, ...

Then from the current directory:

Install required dependencies:

    $ yarn
	
Run typescript on the differents scipts under src/ using ts-node. There is a short description at the beginning of every script that explains its usage:

    $ npx ts-node src/query-balance.ts


To create the Reserve asset on Shibuya Parachain (cfr zombienet config):

    $ npx ts-node src/create-reserve-asset.ts
	2024-02-07 19:48:25        API/INIT: shibuya/120: Not decorating unknown runtime apis: 0xe8accb82fb152951/1
	Shibuya Testnet: most recent block #1963
	Registering asset_id 2823413
	assets.create ( 2823413,ajYMsCKsEAhEvHpeA4XqsfiA9v1CdzZPrCfS6pEfeGHW9j8,10 ) ->  {"inBlock":"0x1c5d7be5fb09e3b7b1d4b85094d185053863c389d095abf5daaf4695d5c64e9e"}
	assets.setTeam ( 2823413,ajYMsCKsEAhEvHpeA4XqsfiA9v1CdzZPrCfS6pEfeGHW9j8,ajYMsCKsEAhEvHpeA4XqsfiA9v1CdzZPrCfS6pEfeGHW9j8,ajYMsCKsEAhEvHpeA4XqsfiA9v1CdzZPrCfS6pEfeGHW9j8 ) ->  {"inBlock":"0xf5e954c9372f35e1ef10913118fd3d691a2f432f9b60e2595df5b4d7b0f24ee5"}
	assets.setMetadata ( 2823413,0x54455354,0x545354,10 ) ->  {"inBlock":"0x1aaac8d951b87b2549048433fa9af4307c0128778cab82db2ace293cd28cf8c4"}
	XCM Reserve Asset Location { v3: { parents: 1, interior: 'Here' } }
	sudo api.tx.xcAssetConfig.registerAssetLocation ( {"v3":{"parents":1,"interior":{"here":null}}},2823413 ) ->  {"inBlock":"0x0dffe8656d366197ae3999e8b94da74f6d82436077cfb0a7572ce130b2b7cc2a"}
	sudo api.tx.xcAssetConfig.setAssetUnitsPerSecond ( {"v3":{"parents":1,"interior":{"here":null}}},100 ) ->  {"inBlock":"0xbcce433d4e143d50689f183c4a7d183539163d4a3d94add0c3d25dd09c1b5929"}
	Completed!


## Targetted node

The node is specified in the beginning of the main() function of the script

  	const wsProvider = new WsProvider('ws://localhost:9946/'); 	

