/*
 * Create Pool
 *
 *  change activeAccount for another owner of the pool
 */
import { BN } from '@polkadot/util';

namespace create_pool {	
    const { ApiPromise,  WsProvider, Keyring,
	    MultiLocationV2, Junctions, 
	  } = require('@polkadot/api');
    
    const { sudo, api_tx, api_tx_ev, batch_tx
	  } = require('./include/curry-api');
    
    async function printChainState(api: any) {
	const chain = await api.rpc.system.chain();
	const lastHeader = await api.rpc.chain.getHeader();
	console.log(`${chain}: most recent block #${lastHeader.number}`);
    }

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }
    
    async function main() {
	// Construct
	//const wsProvider = new WsProvider('ws://localhost:9946/');
	// relay chain:
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;
	
	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice');
	const bob = keyring.addFromUri('//Bob');
	const charlie = keyring.addFromUri('//Charlie');
	const eve =  keyring.addFromUri('//Eve');
	const ferdie = keyring.addFromUri('//Ferdie');
	
	const activeAccount = alice;
	
	await printChainState(api);

	const poolName = "test-pool";

	//const allPoolIds = (await api.query.nominationPools.bondedPools.entries()).filter(([_, bondedPool]) => bondedPool.unwrap().state.isOpen).map(([id, _]) => id.args[0]);
	//console.log(allPoolIds);
	
	var nextPoolId = await api.query.nominationPools.counterForBondedPools();
	nextPoolId = nextPoolId.add(new BN(1));

	const chainDecimals = new BN(api.registry.chainDecimals[0]);
	const bondValue = new BN("10").pow(chainDecimals);

	console.log("Creating pool: ", nextPoolId, ", by :", activeAccount.address, ", bonding: ", bondValue.toString(), "u128" );
	
	await batch_tx("pool create+setMetadata", api, activeAccount, [
	    api.tx.nominationPools.create( bondValue, activeAccount.address, activeAccount.address, activeAccount.address),
	    api.tx.nominationPools.setMetadata(nextPoolId, poolName)]);

	// find most recently awarded validators and add them to pool
	const activeEra =  await api.query.staking.activeEra();
	const lastEra = await api.query.staking.erasRewardPoints(activeEra.unwrap().index.sub(new BN(1)));

	const selectedValidators : any[] = [];
	lastEra.individual.forEach((p, i) => {
	    selectedValidators.push(i);
	    console.log("Adding: ", i.toHuman());
	})

	await api_tx_ev("nominate validators", api, activeAccount, api.tx.nominationPools.nominate(nextPoolId, selectedValidators));
	

	
	
	
	await delay(40000);
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })



    

};
