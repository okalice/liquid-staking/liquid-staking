import { BN } from '@polkadot/util';

namespace staking_status {

    const { ApiPromise,  WsProvider, Keyring, KeyringPair,
	    MultiLocationV2, Junctions, 
	  } = require('@polkadot/api');
    
    const { sudo, api_tx, api_tx_ev, batch_tx
	  } = require('./include/curry-api');

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }

    const accounts = require('./test_accounts.json');
    const keys : typeof KeyringPair[] = [];

    function generate_keys(keyring) {
	accounts.seeds.forEach( (seed) => {
	    const key = keyring.addFromUri(seed);
	    keys.push(key);
	});
    }

    async function showStatus(api: typeof ApiPromise) {
	for (let i = 0; i < keys.length; i++) {
	    const data = await api.query.system.account(keys[i].address);
	    console.log(
		`Account #${i} has ${data.data.free.toHuman()} free balance, [nonce ${data.nonce} / ${data.providers} providers], ledger? ${(await api.query.staking.ledger(keys[i].address)).isSome}, nominator? ${(await api.query.staking.nominators(keys[i].address)).isSome}`);
	};
	//await api.disconnect();
    }    
    
    async function main() {
	// Construct
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });
	generate_keys(keyring);

	// Get general information about the node we are connected to
	const [chain, nodeName, nodeVersion] = await Promise.all([
		api.rpc.system.chain(),
		api.rpc.system.name(),
		api.rpc.system.version()
	]);
	console.log(`🌎 Connected to ${chain} using ${nodeName} v${nodeVersion}`);

	await showStatus(api);


	
	const chainDecimals = new BN(api.registry.chainDecimals[0]);
	const stakeValue = new BN("10").pow(chainDecimals).mul(new BN("20"));

	// every test user joins last created pool
	const lastPoolId = await api.query.nominationPools.counterForBondedPools();
	for(let i = 0; i < keys.length; i++) {
	    api_tx_ev("staking for user" + i.toString(), api, keys[i], api.tx.nominationPools.join(stakeValue, lastPoolId));
	}
	
	await delay(40000);
	
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })
    
    


    
}
