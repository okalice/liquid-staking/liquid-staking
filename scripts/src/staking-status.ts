import { BN } from '@polkadot/util';

namespace staking_status {

    const { ApiPromise,  WsProvider, Keyring,
	    MultiLocationV2, Junctions, 
	  } = require('@polkadot/api');
    
    const { sudo, api_tx, batch_tx
	  } = require('./include/curry-api');

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }


    
    async function main() {
	// Construct
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });


	// Get general information about the node we are connected to
	const [chain, nodeName, nodeVersion] = await Promise.all([
		api.rpc.system.chain(),
		api.rpc.system.name(),
		api.rpc.system.version()
	]);
	console.log(`🌎 Connected to ${chain} using ${nodeName} v${nodeVersion}`);

	console.log(
	    `📈 nominators: ${await api.query.staking.counterForNominators()} / ${await api.query.staking.maxNominatorsCount()}`
	);
	console.log(
	    `📉 validators: ${await api.query.staking.counterForValidators()} / ${await api.query.staking.maxValidatorsCount()}`
	);
	console.log(
	    `🏊 ${await api.query.nominationPools.counterForBondedPools()} nomination pools, ${await api.query.nominationPools.counterForPoolMembers()} members.`
	);


	
	
	await delay(20000);
	
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })
    
    


    
}
