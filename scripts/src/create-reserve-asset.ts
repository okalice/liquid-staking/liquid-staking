

namespace create_reserve_asset {
    const { ApiPromise,  WsProvider, Keyring,
	    MultiLocationV2, Junctions, 
	  } = require('@polkadot/api');

    const { sudo, api_tx, batch_tx
	  } = require('./include/curry-api');
    


    async function printBalance(api: any, address: string) {
	const { nonce, data: balance } = await api.query.system.account(address);
	console.log(address, ": has balance of", Number(balance.free), "and a nonce of", Number(nonce));
    }

    async function printChainState(api: any) {
	const chain = await api.rpc.system.chain();
	const lastHeader = await api.rpc.chain.getHeader();
	console.log(`${chain}: most recent block #${lastHeader.number}`);
    }	

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }

    
    async function main() {
	// Construct
	const wsProvider = new WsProvider('ws://localhost:9946/'); 	
	//const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });


	// Get general information about the node we are connected to
	const [chain, nodeName, nodeVersion] = await Promise.all([
		api.rpc.system.chain(),
		api.rpc.system.name(),
		api.rpc.system.version()
	]);
	console.log(`🌎 Connected to ${chain} using ${nodeName} v${nodeVersion}`);

	const assetId = Math.floor(Math.random() * 10000000) + 10;
 	console.log("Registering assetId", assetId);

	await batch_tx("Asset create, set team and metadata", api, alice, [
	    api.tx.assets.create(assetId, alice.address, 10),
	    api.tx.assets.setTeam(assetId, alice.address, alice.address, alice.address),
	    api.tx.assets.setMetadata(assetId, "TEST", "TST", 10),
	]);
	
	const assetLocation = JSON.parse(`{"v3":{"parents":1,"interior": "Here" }}`);
	console.log("XCM Reserve Asset Location", assetLocation);
	// await sudo("sudo api.tx.xcAssetConfig.registerAssetLocation", api, alice,
	//  	   api.tx.xcAssetConfig.registerAssetLocation( assetLocation, assetId )
	//  	  );

	// await sudo("sudo api.tx.xcAssetConfig.setAssetUnitsPerSecond", api, alice,
	// 	   api.tx.xcAssetConfig.setAssetUnitsPerSecond( assetLocation, 100)
	// 	  );
	
	await sudo("sudo batch api.tx.xcAssetConfig.setAssetLocation * setAssetUnitsPerSecond", api, alice,
		   api.tx.utility.batch([
	 	       api.tx.xcAssetConfig.registerAssetLocation( assetLocation, assetId ),
		       api.tx.xcAssetConfig.setAssetUnitsPerSecond( assetLocation, 100),		       
		   ]));


	
	const assetIdToLocation = await api.query.xcAssetConfig.assetIdToLocation(assetId);
	const location = JSON.stringify(assetIdToLocation);

	console.log("Asset Location", location);

	const assetLocationUnitsPerSecond = await api.query.xcAssetConfig.assetLocationUnitsPerSecond(assetLocation);
	const units = JSON.stringify(assetLocationUnitsPerSecond);

	console.log("Asset Units Per Second", units);

	await delay(20000);
	
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })
    
    
}
