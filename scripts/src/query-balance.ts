/*
 * Basic example to use api to query balance for two accounts in two different ways
 */


namespace query_balance {

const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api');


async function printBalance(api: any, address: string) {
    const { nonce, data: balance } = await api.query.system.account(address);
    console.log(address, ": has balance of", Number(balance.free), "and a nonce of", Number(nonce));
}

async function main() {
    // Construct
    const wsProvider = new WsProvider('ws://localhost:9945/'); 
    const api = await ApiPromise.create({ provider: wsProvider });

    console.log("typeof api:", typeof api);

    // Balance of fixed address
    const MY_ADDR = "5FgXyNWEQ2Yd3rq9SEyxM6MwJC4u6bRRmKn33NZY1GzZAAeP";
    await printBalance(api, MY_ADDR);
    
    // Balance through keyring of preconfigured dev account
    const keyring = new Keyring({ type: 'sr25519'});

    // // (Advanced, development-only) add with an implied dev seed and hard derivation
    const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
    //console.log(alice.toJson());

    await printBalance(api, alice.address);
}

main().then(() => {
    console.log("Completed!");
    process.exit();
})

}
