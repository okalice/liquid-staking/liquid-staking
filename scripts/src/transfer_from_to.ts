import { BN } from '@polkadot/util';

namespace transferFromTo {

    const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api');
    const { api_tx_ev } = require('./include/curry-api');
    
    const {AbiMessage} = require('@polkadot/api-contract/types');

    async function printChainState(api: any) {
	const chain = await api.rpc.system.chain();
	const lastHeader = await api.rpc.chain.getHeader();
	console.log(`${chain}: last block #${lastHeader.number}`);
    }	


    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }    
    
    async function main() {
	// Construct
	//const wsProvider = new WsProvider('ws://localhost:9946/'); 	
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });
	
	await printChainState(api);


	//want to transfer 2 Units from Alice to Bob

	const chainDecimals = new BN(api.registry.chainDecimals[0]);
	const transferValue = new BN("10").pow(chainDecimals).mul(new BN("2"));
					      
	console.log("Transfer from Alice to Bob: ", transferValue, "u128");

	await api_tx_ev("transfer alice -> bob", api, alice, api.tx.balances.transferAllowDeath(bob.address, transferValue));
	

    }
    
    main().then(() => {
	
	console.log("Completed!");
	process.exit();
    })
    

    


}
