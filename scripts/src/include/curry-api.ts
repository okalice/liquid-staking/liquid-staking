/**
 * Helper functions for calling extrincics via polkadot-js - Zabux - 2024
 *      - blocks until completion
 *      - reports and stops in case of on/off-chain error
 *      - return Transaction result
 *
 *  Usage:
 *    await api_tx("> balances transfer", api, alice, api.tx.balances.transferAllowDeath(bob, amount));
 *    await sudo("sudo api.tx.xcAssetConfig.registerAssetLocation", api, alice,
 *         api.tx.xcAssetConfig.registerAssetLocation( assetLocation, asset_id ));
 *
 */

import '@polkadot/api-augment';
import { ApiPromise, SubmittableResult, Keyring } from '@polkadot/api';
import { KeyringPair } from '@polkadot/keyring/types';
const {  ContractSubmittableResult } = require('@polkadot/api-contract');

export const api_tx = async (
    log_msg: string,
    api: ApiPromise,
    signer: KeyringPair,
    api_call: any
): Promise<SubmittableResult> => {
    return new Promise(async (resolve, reject) => {
	const result = await api_call.signAndSend(signer,
						  (result) =>
	    {

		if (result.status.isInBlock || result.status.isFinalized) {

		    var haveError = false;
		    //explain errors
		    result.events
			.filter(({ event }) =>
			    api.events.system.ExtrinsicFailed.is(event)
			       )
			.forEach(({ event: { data: [error, info] } }) => {
			    if (error.isModule) {
				// for module errors, we have the section indexed, lookup
				const decoded = api.registry.findMetaError(error.asModule);
				const { docs, method, section } = decoded;
				
				console.log(`${section}.${method}: ${docs.join(' ')}`);
			    } else {
				// Other, CannotLookup, BadOrigin, no extra info
				console.log(error.toString());
			    }
			    haveError = true;
			});

		    if(haveError) {
			reject(new Error(`Extrinsic Failed`));
		    }

		    console.log(log_msg, "(", api_call.args.toString(), ") -> ", result.status.toString());			
		    resolve(result);			    
		} else if (result.status.isInvalid) {
		    reject(new Error(`Extrinsic isInvalid`));
		} else if (result.dispatchError) {
		    reject(new Error(`Dispatch error: ${result.dispatchError}`));
		} else if (result.internalError) {
		    reject(new Error(`Internal error: ${result.internalError}`));
		} 
	    });
    });
};

export const api_tx_ev = async (
    log_msg: string,
    api: ApiPromise,
    signer: KeyringPair,
    api_call: any
): Promise<SubmittableResult> => {
    return new Promise(async (resolve, reject) => {
	const result = await api_call.signAndSend(signer,
							  (result) =>
	    {
		if (result.status.isInBlock || result.status.isFinalized) {

		    if(result.status.isFinalized) {
			//print all events
			result.events.forEach(({ phase, event: { data, method, section } }) => {
			    //console.log(`\t ${phase}: ${section}.${method}:: ${data}`);
			    console.log(`\t ${section}.${method}:: ${data}`);			    
			});
		    }
		    
		    //explain errors
		    var haveError = false;
		    result.events
			.filter(({ event }) =>
			    api.events.system.ExtrinsicFailed.is(event)
			       )
			.forEach(({ event: { data: [error, info] } }) => {
			    if (error.isModule) {
				// for module errors, we have the section indexed, lookup
				const decoded = api.registry.findMetaError(error.asModule);
				const { docs, method, section } = decoded;
				
				console.log(`${section}.${method}: ${docs.join(' ')}`);
			    } else {
				// Other, CannotLookup, BadOrigin, no extra info
				console.log(error.toString());
			    }
			    haveError = true;
			});

		    if(haveError) {
			reject(new Error(`Extrinsic Failed`));
		    }
		    
		    console.log(log_msg, "(", api_call.args.toString(), ") -> ", result.status.toString());
		    resolve(result);

		} else if (result.status.isInvalid) {
		    reject(new Error(`Extrinsic isInvalid`));
		} else if (result.dispatchError) {
		    reject(new Error(`Dispatch error: ${result.dispatchError}`));
		} else if (result.internalError) {
		    reject(new Error(`Internal error: ${result.internalError}`));
		} 
	    });
    });
};

export const sudo = async (
    log_msg: string,    
    api: ApiPromise,
    signer: KeyringPair,
    call: any,
): Promise<SubmittableResult> => {
    return new Promise(async (resolve, reject) => {
        const result = await api.tx.sudo
  .sudo(call)
  .signAndSend(signer, ( result ) => {
    switch (result.status.type) {
      case "Finalized":
      case "InBlock":
        {
          if (result.status.isInBlock) {
              console.log(log_msg, result.status.asInBlock.toString());
          }

	    var haveError = false;
            result.events.forEach((record) => {
		if (api.events.sudo.Sudid.is(record.event)) {

		    // NEW PART:
		    if (record.event.data.sudoResult.isErr) {

			// SAME PART AS IN DOCS:
			let error = record.event.data.sudoResult.asErr;
			if (error.isModule) {
			    // for module errors, we have the section indexed, lookup
			    const decoded = api.registry.findMetaError(error.asModule);
			    const { docs, name, section } = decoded;

			    console.log(`${section}.${name}: ${docs.join(" ")}`);
			} else {
			    // Other, CannotLookup, BadOrigin, no extra info
			    console.log(error.toString());
			}
			haveError = true;
		    } else {
			console.log(`\t ${record.event.section}.${record.event.method}:: ${record.event.data}`);			    

		    }
			
		}
            });
	    
	    if(haveError) {
		reject(new Error(`Extrinsic Failed`));
	    }

	    console.log(log_msg, "(", call.args.toString(), ") -> ", result.status.toString());
	    resolve(result);
        }
            break;
	default:
            break;
    }
  })});
}


export const batch_tx = async (
    log_msg: string,
    api: ApiPromise,
    signer: KeyringPair, 
    call: any,
): Promise<SubmittableResult> => {
    return new Promise(async (resolve, reject) => {
	const result = await api.tx.utility.batch(call).signAndSend(signer,
							   (result) =>
	    {		    
		if (result.status.isInBlock || result.status.isFinalized) {
		    
		    if(result.status.isFinalized) {
			//print all events
			result.events.forEach(({ phase, event: { data, method, section } }) => {
			    console.log(`\t' ${phase}: ${section}.${method}:: ${data}`);
			});
		    }
		    
		    console.log(log_msg, " -> ", result.status.toString());			
		    resolve(result);

		} else if (result.status.isInvalid) {
		    reject(new Error(`Extrinsic isInvalid`));
		} else if (result.dispatchError) {
		    reject(new Error(`Dispatch error: ${result.dispatchError}`));
		} else if (result.internalError) {
		    reject(new Error(`Internal error: ${result.internalError}`));
		}
	    });					       
	
							       
    });
};







