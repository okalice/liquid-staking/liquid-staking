
namespace staking_status {

    const { ApiPromise,  WsProvider, Keyring, KeyringPair,
	    MultiLocationV2, Junctions, 
	  } = require('@polkadot/api');
    
    const { sudo, api_tx, batch_tx
	  } = require('./include/curry-api');

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }

    const accounts = require('./test_accounts.json');
    const keys : typeof KeyringPair[] = [];

    function generate_keys(keyring) {
	accounts.seeds.forEach( (seed) => {
	    const key = keyring.addFromUri(seed);
	    keys.push(key);
	});
    }
    
    
    async function main() {
	// Construct
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });
	generate_keys(keyring);

	// Get general information about the node we are connected to
	const [chain, nodeName, nodeVersion] = await Promise.all([
		api.rpc.system.chain(),
		api.rpc.system.name(),
		api.rpc.system.version()
	]);
	console.log(`🌎 Connected to ${chain} using ${nodeName} v${nodeVersion}`);

	
	await delay(20000);
	
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })
    
    


    
}
