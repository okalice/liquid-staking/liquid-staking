import { BN } from '@polkadot/util';

namespace staking_status {

    const { ApiPromise,  WsProvider, Keyring, KeyringPair,
	    MultiLocationV2, Junctions,
	    SubmittableExtrinsic, ApiType, 
	    
	  } = require('@polkadot/api');
    
    const { sudo, api_tx, batch_tx
	  } = require('./include/curry-api');

    function delay(ms: number) {
	return new Promise( resolve => setTimeout(resolve, ms) );
    }

    const accounts = require('./test_accounts.json');

    async function main() {
	// Construct
	const wsProvider = new WsProvider(); 	
	const api = await ApiPromise.create({ provider: wsProvider });
	await api.isReady;

	const keyring = new Keyring({ type: 'sr25519'});
	const alice = keyring.addFromUri('//Alice', { name: 'Alice default' });
	const bob = keyring.addFromUri('//Bob', { name: 'Bob default' });


	// Get general information about the node we are connected to
	const [chain, nodeName, nodeVersion] = await Promise.all([
		api.rpc.system.chain(),
		api.rpc.system.name(),
		api.rpc.system.version()
	]);
	console.log(`🌎 Connected to ${chain} using ${nodeName} v${nodeVersion}`);

	const chainDecimals = new BN(api.registry.chainDecimals[0]);
	const transferValue = new BN("10").pow(chainDecimals).mul(new BN("20"));

	
	const keys : typeof KeyringPair[] = [];
	accounts.seeds.forEach( (seed) => {
	    const key = keyring.addFromUri(seed);
	    console.log("Test account from seed:", key.address);
	    keys.push(key);
	});

	//const transferBatch : typeof SubmittableExtrinsic<typeof ApiType>[] = [];
	const transferBatch : any[] = [];
	for(let i = 0; i < keys.length; i++) {
	    transferBatch.push(
		api.tx.balances.transferAllowDeath(keys[i].address, transferValue)	    
	    )
	}

	batch_tx("batch transfer alice -> testaccounts", api, alice, transferBatch);

	await delay(20000);
	
    }
    
    main().then(() => {
	console.log("Completed!");
	process.exit();
    })
    
    


    
}
