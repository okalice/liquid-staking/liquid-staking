# Running Shibuya in Zombienet

## Setup

Place the following file in your PATH
- latest zombienet binary https://github.com/paritytech/zombienet
- latest Astar collator binary https://github.com/AstarNetwork/Astar
- latest polkadot binary https://github.com/paritytech/polkadot-sdk

## Run 

    $ zombienet spawn -p native zombienet/single_chain.toml 

