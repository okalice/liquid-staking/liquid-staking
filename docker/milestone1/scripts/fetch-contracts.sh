#!/bin/bash

# Fetch the liquid-staking repo from github

. /tmp/functions.sh

cd
mkdir -p src
cd src

cat > ~/.gitconfig <<EOF
[advice]
	detachedHead=0
EOF

action "Fetching okAlice liquid-staking repo" git clone -q --depth 1 https://gitlab.com/okalice/liquid-staking/liquid-staking.git
