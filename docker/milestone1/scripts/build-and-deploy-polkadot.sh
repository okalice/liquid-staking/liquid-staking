#!/usr/bin/env bash

set -e

sleep 5 # slow start to avoid docker mac error


. /tmp/functions.sh

TARGET=~/src/liquid-staking/artefacts/bin/polkadot

echo
if [ -f $TARGET ]; then
    echo "$TARGET already exists"
    echo "Not rebuilding, exiting..."
    exit
else
    echo "building $TARGET"
fi


rustup target add wasm32-unknown-unknown
rustup component add rust-src

git clone --depth 1  https://gitlab.com/okalice/polkadot-sdk.git
#git clone --depth 1 --branch polkadot-v1.7.0 https://github.com/paritytech/polkadot-sdk.git
cd polkadot-sdk
cargo build --release --features fast-runtime

mkdir -p $(dirname ${TARGET})
cp target/release/polkadot* $(dirname ${TARGET})

echo "Build complete: $TARGET"
ls -al  $(dirname ${TARGET})
