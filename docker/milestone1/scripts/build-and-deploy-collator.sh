#!/usr/bin/env bash

# build and deploy the liquid staking Astar Collator

set -e

. /tmp/functions.sh

cd
cd src

TARGET=~/src/liquid-staking/artefacts/bin/astar-collator

echo
if [ -f $TARGET ]; then
    echo "$TARGET already exists"
    echo "Not rebuilding, exiting..."
    exit
else
    echo "building $TARGET"
fi

#git clone --depth 1 --branch v5.32.1 https://github.com/AstarNetwork/Astar.git
git clone --depth 1 https://github.com/ok-Alice/Astar.git

cd Astar
cargo build --package astar-collator --release


mkdir -p $(dirname ${TARGET})
cp target/release/astar-collator ${TARGET}

echo "Build complete: $TARGET"
ls -al  $(dirname ${TARGET})
