# ADR 4. Issuer-staker staking operation

Date: 2024-02-22

## Status 

Proposed

## Context

The contract exists on the parachain, but the user can have native
tokens on the relaychain or the wrapped version on the parachain.

## Decision

The contract will assume that the user has a wrapped version of the
navtive relaychain-tokens on the parachain. In the other case, the UI
will provide the possibility to do a reserve asset transfer.

### Use case: reserve transfer

**Goal**: User has wrapped tokens on parachain

**Precondition**: User owns native tokens on the relay chain

**Flow**:
- User calls *transfer assets* on UI
- UI performs *reserve asset transfer* for User

**Details**:
- UI performs call to tx.xcmPallet.reserveTransferAssets
- e.g: 0x630203000100411f0300010100d43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d0304000000000b00407a10f35a00000000

### Use case: stake

**Goal**: User stakes desired amount of tokens

**Precondition**: User has wrapped tokens on parachain

**Flow**:
- Via UI user calls stake on contract
- contract transfers asset from user account to contract contract does
  reserve asset transfer to a sovereign account of the contract on the
  relaychain
- contract calls nomination join (1st time) or bond extra
- user recieves ldot

### Use case: reserve transfer back

**Goal**: Convert wrapped tokens back to relaychain native tokens

**Precondition**: User owns wrapped tocken on parachain

**Flow**: 

