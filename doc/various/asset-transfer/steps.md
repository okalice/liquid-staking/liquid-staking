## Relay chain to Astar
- Build polkadot node with fast runtime feature
  `cargo b -r --features=fast-runtime`

- Register asset xcDOT on astar with id 1000.

- Register dot asset on Astar as a cross chain asset.
  Encoded Call Data: `0x63003600030100a10f`
- 0x3600030100a10f

- Register amount of unit to charge per second
  `0x63003601030100a10f`

- Convert address of alice to hex (Dev > Utilities in pjs)
  `0xd43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d`

- Transfer 100 westies from westend to Shibuya
  `0x630203000100411f0300010100d43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d0304000000000b00407a10f35a00000000`

Para 2000 sovereign account would have got 100 westies: `5Ec4AhPUwPeyTFyuhGuBbD224mY85LKLMSqSSo33JYWCazU4`

- Transfer to account: 5FWR2qSK9rpr19DzpwiGudhGRorCh1go6iU7DAzt8cyxw1va
    `0x370103000100000b00a0724e18090301010100985071a17e8997cd8d8520f2c7d9c690100b1544e31179f844080a24f853947400`

- Create pool from Astar to Relay with
  - Np call BOB
    `0x1d060f0080c6a47e8d03008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48`
  - Full call
    `0x3300030100030c0004000000000b00a0724e180913000000000b00a0724e18090006010700e87648170700e8764817b5011d060f0080c6a47e8d03008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48008eaf04151687736326c9fea17e25fc5287613693c912909cb226aa4794f26a48`


- Issue with Pool creation from Shibuya to Relay chain:
When an interior location of a parachain (such as a contract or a user account) sends an xcm to the relay chain, the origin is multi-location of pattern
X2(para_id, account_id). This conversion is not supported in relay chain and fails with `xcm::execute: !!! ERROR: FailedToTransactAsset("AccountIdConversionFailed")`.

Is this gonna be supported? If not, how do we handle this? 


```
  - xcm::process_instruction: === WithdrawAsset(MultiAssets([MultiAsset { id: Concrete(MultiLocation { parents: 0, interior: Here }), fun: Fungible(10000000000000) }]))
  - xcm::currency_adapter: withdraw_asset what: MultiAsset { id: Concrete(MultiLocation { parents: 0, interior: Here }), fun: Fungible(10000000000000) }, who: MultiLocation { parents: 0, interior: X2(Parachain(2000), AccountId32 { network: Some(Westend), id: [212, 53, 147, 199, 21, 253, 211, 28, 97, 20, 26, 189, 4, 169, 159, 214, 130, 44, 133, 88, 133, 76, 205, 227, 154, 86, 132, 231, 165, 109, 162, 125] }) }
  - xcm::execute: !!! ERROR: FailedToTransactAsset("AccountIdConversionFailed")
  - xcm::execute_xcm_in_credit: result: Err(ExecutorError { index: 1, xcm_error: FailedToTransactAsset("AccountIdConversionFailed"), weight: Weight { ref_time: 100015189000, proof_size: 100000000000 } })
  - xcm::execute_xcm_in_credit: Execution errored at 1: FailedToTransactAsset("AccountIdConversionFailed") (original_origin: MultiLocation { parents: 0, interior: X1(Parachain(2000)) })
  - xcm::process-message: XCM message execution incomplete, used weight: Weight(ref_time: 153499000, proof_size: 3593), error: FailedToTransactAsset("AccountIdConversionFailed")
```


 