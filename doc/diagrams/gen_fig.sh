#!/bin/bash

JAR=plantuml/plantuml-gplv2-1.2024.0.jar

for SFILE in uml/*; do
    bname=$(basename $SFILE)
    echo "Genearting diagram for $SFILE"
    DFILE="fig/${bname}.png"

    cat $SFILE | java -Djava.awt.headless=true -jar $JAR -pipe > $DFILE
done
